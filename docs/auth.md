#Login

Usado para coletar o token de um usuário registrado.

**URL** : `/login/`

**Método** : `POST`

**Autenticação requerida** : Não

**Nível de autorização** : Nenhum

**Campos requeridos** : `username | password`

**Corpo da requisição:**

```json
{
    "username": "usuario",
    "password": "senha"
}
```

**Exemplo**

```
curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ \ 
   "username": "admin", \ 
   "password": "admind123", \ 
 }' 'http://localhost:8080/login'
```

###Sucesso

**Código** : `204 NO CONTENT`

**Resposta [Header] :**

```
authorization : Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjQxNzk3N30.tF9sWgEc5GCP8Iy85qQNFHlByK1hSftKJGCH8bZgEkCPdhdow4_Z6AEqyBFnz0ROsW4kEzFa_hFwqLfP3zCkZg
```

###Erro

**Condição** : Se usuário ou senha forem inválidos

**Códido** : `401 UNAUTHORIZED`

**Resposta [Body] :**

```json
{
    "timestamp": 1536365180873,
    "status": 401,
    "error": "Não autorizado",
    "message": "Bad credentials",
    "path": "/login"
}
```

#Refresh

Usado para coletar um novo token valido.

**URL** : `/auth/refresh_token/`

**Método** : `GET`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_USER

**Exemplo :**

```
curl -X GET --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjQxNzk3N30.tF9sWgEc5GCP8Iy85qQNFHlByK1hSftKJGCH8bZgEkCPdhdow4_Z6AEqyBFnz0ROsW4kEzFa_hFwqLfP3zCkZg' \
'http://localhost:8080/auth/refresh_token'
```

###Sucesso

**Código** : `204 NO CONTENT`

**Resposta [Header]:**

```
authorization : Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjQxNzk3N30.tF9sWgEc5GCP8Iy85qQNFHlByK1hSftKJGCH8bZgEkCPdhdow4_Z6AEqyBFnz0ROsW4kEzFa_hFwqLfP3zCkZg
```

---

#Erros comuns

**Condição** : Se o token informado for inválido

**Códido** : `403 FORBIDDEN`

**Resposta [Body]:**

```json
{
    "timestamp": "2018-09-08T02:35:48.794+0000",
    "status": 403,
    "error": "Forbidden",
    "message": "Access Denied",
    "path": "/camiho/do/recurso"
}
```

---

**Condição** : Se o usuário não possuir acesso ao recurso.

**Códido** : `403 FORBIDDEN`

**Resposta [Body] :**

```json
{
    "timestamp": "2018-09-08T15:30:31.920+0000",
    "status": 403,
    "error": "Forbidden",
    "message": "Forbidden",
    "path": "/camiho/do/recurso"
}
```