#Exibir Usuário

A api permite acesso à informações do usuário de acordo com seu nível de acesso. Para exibir informações do usuário
atual basta apenas estar autenticado, para acessar as informações de outros usuários é necessário nível de acesso
administrativo.

**URL** : `/usuario/{id}`

**Método** : `GET`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_USER

**Exemplo**

```
curl -X GET --header 'Accept: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjQxNzk3N30.tF9sWgEc5GCP8Iy85qQNFHlByK1hSftKJGCH8bZgEkCPdhdow4_Z6AEqyBFnz0ROsW4kEzFa_hFwqLfP3zCkZg' \
'http://localhost:8080/usuario/2'
```

###Sucesso

**Código** : `200 OK`

**Resposta [Body] :**

```json
{
    "id": 2,
    "username": "severino",
    "pessoa": {
        "id": 1,
        "nome": "Severino Heitor Teixeira",
        "cpf": "123.456.789-10",
        "dataNascimento": "1996-11-02T03:00:00.000+0000"
    },
    "authorities": [
        {
            "authority": "ROLE_USER"
        }
    ]
}
```

#Criar usuário

Este recursos permite a criação de novos usuários, porém, está disponível apenas para usuários que possuem nível de 
acesso administrativo.

**URL** : `/usuario`

**Método** : `POST`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_ADMIN

**Campos requeridos** : `username, password`

**Corpo da requisição:**

```json
{
    "username": "usuario",
    "password": "senha",
    "roles": [ "ROLE_ADMIN", "ROLE_USER" ]
}
```

**Exemplo** :

```
curl -X POST --header 'Content-Type: application/json' --header \
 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
 -d '{ \ 
   "username": "joao", \ 
   "password": "senha321", \ 
   "roles" : [ "ROLE_ADMIN", "ROLE_USER" ] \
 }' 'http://localhost:8080/usuario'
```

###Sucesso

**Código** : `201 CREATED`

**Resposta [Header] :**

``
location : http://localhost:8080/usuario/{id}
``

###Erro

**Condição** : Se o username informado já existir.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "status": 400,
    "message": "Username ja utilizado",
    "timeStamp": 1536419829348
}
```
---

**Condição** : Se algun campo obrigatório não for informado.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "timestamp": "2018-09-08T15:18:32.227+0000",
    "status": 400,
    "error": "Bad Request",
    "errors": [
        {
            "codes": [
                "NotEmpty.usuario.password",
                "NotEmpty.password",
                "NotEmpty.java.lang.String",
                "NotEmpty"
            ],
            "arguments": [
                {
                    "codes": [
                        "usuario.password",
                        "password"
                    ],
                    "arguments": null,
                    "defaultMessage": "password",
                    "code": "password"
                }
            ],
            "defaultMessage": "não pode estar vazio",
            "objectName": "usuario",
            "field": "password",
            "rejectedValue": null,
            "bindingFailure": false,
            "code": "NotEmpty"
        }
    ],
    "message": "Validation failed for object='usuario'. Error count: 1",
    "path": "/usuario"
}
```

#Listar usuários

Este recursos permite a listagem usuários cadastrados, porém, está disponível apenas para usuários que possuem nível de 
acesso administrativo.

**URL** : `/usuario/listar`

**Método** : `GET`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_ADMIN

**Exemplo**

```
curl -X GET --header 'Accept: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
 'http://localhost:8080/usuario/listar'
```

###Sucesso

**Código** : `201 CREATED`

**Resposta [Body] :**

```json
[
    {
        "id": 1,
        "username": "admin",
        "pessoa": null,
        "authorities": [
            {
                "authority": "ROLE_ADMIN"
            },
            {
                "authority": "ROLE_USER"
            }
        ]
    },
    {
        "id": 2,
        "username": "severino",
        "pessoa": {
            "id": 1,
            "nome": "Severino Heitor Teixeira",
            "cpf": "123.456.789-10",
            "dataNascimento": "1996-11-02T03:00:00.000+0000"
        },
        "authorities": [
            {
                "authority": "ROLE_USER"
            }
        ]
    },
    {
        "id": 3,
        "username": "isis",
        "pessoa": {
            "id": 2,
            "nome": "Isis Betina Aragão",
            "cpf": "496.511.510-48",
            "dataNascimento": "1984-01-15T03:00:00.000+0000"
        },
        "authorities": [
            {
                "authority": "ROLE_USER"
            }
        ]
    }
]
```

#Atualizar usuarios

Este recursos permite a atualziação de dados cadastrais dos usuários, ele também é disponibilizado de acordo 
com o nível de acesso. Para usuário que não possuem acesso administrativo, é possível atualizar apenas os próprios
dados.

**URL** : `/usuario/{id}`

**Método** : `PUT`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_USER

**Corpo da requisição:**

```json
{
    "username": "usuario",
    "password": "senha",
    "roles": [ "ROLE_ADMIN", "ROLE_USER" ]
}
```

**Exemplo**

```
curl -X PUT --header 'Content-Type: application/json' --header \
 'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
 -d '{ \ 
   "username": "joao", \ 
   "password": "senha321", \ 
   "roles" : [ "ROLE_ADMIN", "ROLE_USER" ] \
 }' 'http://localhost:8080/usuario'
```

###Sucesso

**Código** : `204 NO CONTENT`

###Erro

**Condição** : Se o username informado já existir.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "status": 400,
    "message": "Username ja utilizado",
    "timeStamp": 1536419829348
}
```

#Excluir usuário

Este recurso permite a exclusão de contas de usuário, caso o mesmo não esteja vinculado a uma pessoa. Está disponínel
apenas para usuários que possuem acesso administrativo.

**URL** : `/usuario/{id}`

**Método** : `DELETE`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_ADMIN

**Exemplo**

```
curl -X DELETE --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
'http://localhost:8080/usuario/3'
```

###Sucesso

**Código** : `204 NO CONTENT`

###Erro

**Condição** : Se o usuário estiver vinculado a uma pessoa.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "status": 400,
    "message": "Nao e possível excluir um usuario vinculado a uma pessoa",
    "timeStamp": 1536419829348
}
```




