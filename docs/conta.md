#Exibir conta

Este recurso permite a visualização dos dados da conta vinculada a um usuário. O acesso a este recurso é liberado
de acordo com seu nível de acesso, apenas administradores podem filtrar contas que não estão vinculadas a si mesmo.

**URL** : `/conta/{id}`

**Método** : `GET`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_USER

**Exemplo**

```
curl -X GET --header 'Accept: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjQxNzk3N30.tF9sWgEc5GCP8Iy85qQNFHlByK1hSftKJGCH8bZgEkCPdhdow4_Z6AEqyBFnz0ROsW4kEzFa_hFwqLfP3zCkZg' \
'http://localhost:8080/conta/1'
```

###Sucesso

**Código** : `200 OK`

**Resposta [Body] :**

```json
{
    "id": 1,
    "saldo": 200,
    "limiteSaqueDiario": 1200,
    "ativo": true,
    "dataCriacao": "2018-09-08T20:07:18.650+0000",
    "tipoConta": "POUPANCA",
    "pessoa": {
        "id": 1,
        "nome": "Severino Heitor Teixeira",
        "cpf": "123.456.789-10",
        "dataNascimento": "1996-11-02T03:00:00.000+0000"
    }
}
```

#Criar conta

Este recurso permite a criacao de uma conta e seus respectivos dados cadastrais, além disso, a vinculação a um usuário. 
Disponível apenas para usuários com nível de acesso administrativo.

**URL** : `/conta`

**Método** : `POST`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_ADMIN

**Campos obrigatórios** : `saldo, limiteSaqueDiario, tipoConta, pessoa.id`

**Corpo da requisição** : 

```json
{
    "saldo": 99.99,
    "limiteSaqueDiario": 999.99,
    "tipoConta": 0, //Valores aceitáveis: 1 = CORRENTE | 2 = POUPANCA
    "pessoa": {
        "id": 1,
        "nome": "nome",
        "cpf": "cpf",
        "dataNascimento": "yyyy-MM-dd",
        "usuario": {
             "id": 1
        }
    }
}
```

**Exemplo** :

```
curl -X POST --header 'Content-Type: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
-d '{ \ 
   "saldo": 500.00, \ 
   "limiteSaqueDiario": 1000.00, \ 
   "tipoConta": "POUPANCA" \
   "pessoa": { \
       "id": 1, \
       "nome": "Severino Heitor Teixeira", \
       "cpf": "123.456.789-10", \
       "dataNascimento": "1996-11-02" \
       "usuario": { \
            "id": 1 \
       } \
   } \
 }' 'http://localhost:8080/conta'
```

###Sucesso

**Código** : `201 CREATED`

**Resposta [Header] :**

```
location : http://localhost:8080/conta/1
```

###Erro

**Condição** : Se o cpf informado já existir.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "status": 400,
    "message": "Cpf ja utilizado",
    "timeStamp": 1536448364893
}
```
---
**Condição** : Se não houver dados de pessoa.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "status": 400,
    "message": "Nenhum pessoa foi informada",
    "timeStamp": 1536448364893
}
```
---
**Condição** : Se não houver registro de pessoa e o usuário não for informado.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "status": 400,
    "message": "Nenhum usuario foi informado",
    "timeStamp": 1536448364893
}
```
---
**Condição** : Se os dados informados forem inválidos ou estiverem faltando.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "timestamp": "2018-09-09T00:15:45.935+0000",
    "status": 400,
    "error": "Bad Request",
    "errors": [
        {
            "codes": [
                "NotNull.conta.tipoConta",
                "NotNull.tipoConta",
                "NotNull.com.vh.desafio.models.enums.TipoConta",
                "NotNull"
            ],
            "arguments": [
                {
                    "codes": [
                        "conta.tipoConta",
                        "tipoConta"
                    ],
                    "arguments": null,
                    "defaultMessage": "tipoConta",
                    "code": "tipoConta"
                }
            ],
            "defaultMessage": "não pode ser nulo",
            "objectName": "conta",
            "field": "tipoConta",
            "rejectedValue": null,
            "bindingFailure": false,
            "code": "NotNull"
        }
    ],
    "message": "Validation failed for object='conta'. Error count: 1",
    "path": "/conta"
}
```

#Atualizar conta

Este recurso permite desativar ou alterar o limite de saque diário de uma conta. Está disponível apenas para usuários
que possuem acesso admistrativo.

**URL** : `/conta{id}`

**Método** : `PUT`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_ADMIN

**Corpo da requisicao** :

```json
{
  "ativo" : true,
  "limiteSaqueDiario" : 999.99
}
```
**Exemplo**:

```
curl -X PUT --header 'Content-Type: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
-d '{ \ 
   "ativo": true, \ 
   "limiteSaqueDiario": 1000.00, \ 
   } \
 }' 'http://localhost:8080/conta/1'
```


###Sucesso

**Código** : `204 NO CONTENT`

#Excluir conta

Este recurso permite excluir uma conta, desde que, esta não possua transacoes vinculadas a ela. Está disponível apenas para usuários
que possuem acesso admistrativo.

**URL** : `/conta{id}`

**Método** : `DELETE`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_ADMIN

**Exemplo**:

```
curl -X DELETE --header 'Content-Type: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
'http://localhost:8080/conta/1'
```

###Sucesso

**Código** : `204 NO CONTENT`

#Déposito

Este recurso permite realizar operações de deposito de valores em uma conta.

**URL** : `/conta{id}/deposito`

**Método** : `POST`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_USER

**Campos obrigatórios** : `valor`

**Corpo da requisição** : 

```json
{
  "valor" : 9999.99
}
```
**Exemplo**:

```
curl -X POST --header 'Content-Type: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
-d '{ \  
   "valor": 1000.00, \ 
   } \
 }' 'http://localhost:8080/conta/1/deposito'
```

###Sucesso

**Códido** : `201 CREATED`

**Resposta [Header] :** 

```
location : http://localhost:8080/transacao/1
```

#Saque

Este recurso permite realizar operações de saque de valores em uma conta.

**URL** : `/conta{id}/saque`

**Método** : `POST`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_USER

**Campos obrigatórios** : `valor`

**Corpo da requisição** : 

```json
{
  "valor" : 9999.99
}
```
**Exemplo**:

```
curl -X POST --header 'Content-Type: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjUxOTA2MX0.fEHEvLpNLdGg71oQt8cO1g9YtbkydX6ZWavFj1Af-NziYgeYEE-sehyc83VQHHdUcs8YjEDpOqUAsq4QwyjtMA' \
-d '{ \  
   "valor": 1000.00, \ 
   } \
 }' 'http://localhost:8080/conta/1/saque'
```

###Sucesso

**Códido** : `201 CREATED`

**Resposta [Header] :** 

```
location : http://localhost:8080/transacao/2
```

###Erro

**Condição** : Se a conta não possuir saldo suficiente.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "status": 400,
    "message": "Saldo insuficiente",
    "timeStamp": 1536448364893
}
```
---
**Condição** : Se o valor do saque superar o saldo do limite de saque diário.

**Códido** : `400 BAD REQUEST`

**Resposta [Body] :**

```json
{
    "status": 400,
    "message": "Valor da transacao excede o limite diario para saque",
    "timeStamp": 1536448364893
}
```

#Extrato por periodo

Este recurso permite visualizar todas as transacoes efetuadas no periodo informado.

**URL** : `/conta{id}/extrato?dataInicial=yyyy-MM-dd&dataFinal=yyyy-MM-dd&page=0`

**Método** : `GET`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_USER

**Campos obrigatórios** : `dataInicial, dataFinal`

**Exemplo** :

```
curl -X GET --header 'Accept: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjQxNzk3N30.tF9sWgEc5GCP8Iy85qQNFHlByK1hSftKJGCH8bZgEkCPdhdow4_Z6AEqyBFnz0ROsW4kEzFa_hFwqLfP3zCkZg' \
'http://localhost:8080/conta/1/extrato?dataInicial=2018-09-01&dataFinal=2018-09-30'
```

###Sucesso

**Códido** : `200 OK`

**Resposta [Body] :**

```json
{
    "content": [
        {
            "id": 2,
            "valor": 100,
            "dataTransacao": "2018-09-05T19:59:52.056+0000"
        },
        {
            "id": 1,
            "valor": -100,
            "dataTransacao": "2018-09-05T19:59:27.187+0000"
        }
    ],
    "pageable": {
        "sort": {
            "sorted": true,
            "unsorted": false
        },
        "pageSize": 20,
        "pageNumber": 0,
        "offset": 0,
        "paged": true,
        "unpaged": false
    },
    "totalPages": 1,
    "totalElements": 2,
    "last": true,
    "first": true,
    "numberOfElements": 2,
    "sort": {
        "sorted": true,
        "unsorted": false
    },
    "size": 20,
    "number": 0
}
```