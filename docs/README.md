## Recursos abertos

Recursos que não requerem autenticação.

* [Login](auth.md) : `POST /login/`

## Recursos que requisitam autenticação

Estes recursos requerem que um token valido seja inserido no cabeçalho da requisição. 
O token pode ser adquirido utilizando o recurso acima, algumas ainda precisam de nivel
autorização administrativo.

### Atualização de token

Ao adquirir um token, o mesmo possuirá um tempo de expiração, será necessário adquirir um novo token
para dar continuidade ao uso da api.

* [Refresh](auth.md) : `GET /auth/refresh_token`

### Usuario

Cada endpoint exibe ou manipula as informaçẽos relacionadas à usuários:

* [Exibir](usuario.md) : `GET /usuario/{id}`
* [Criar](usuario.md) : `POST /usuario`
* [Atualizar](usuario.md) : `PUT /usuario/{id}`
* [Excluir](usuario.md) : `DELETE /usuario/{id}`

### Conta

Cada endpoint exibe ou manipula as informaçẽos relacionadas à conta bancária:

* [Exibir](conta.md) : `GET /conta/{id}`
* [Criar](conta.md) : `POST /conta`
* [Atualizar](conta.md) : `PUT /conta/{id}`
* [Excluir](conta.md) : `DELETE /conta/{id}`
* [Saque](conta.md) : `POST /conta/{id}/saque`
* [Deposito](conta.md) : `POST /conta/{id}/deposito`
* [Extrato](conta.md) : `POST /conta/{id}/extrato?dataInicial=yyyy-MM-dd&dataFinal=yyyy-MM-dd&page=0`

### Transacao

Cada endpoint exibe ou manipula as informaçẽos relacionadas à transações bancárias:

* [Exibir](transacao.md) : `GET /transacao/{id}`
* [Excluir](transacao.md) : `DELETE /transacao/{id}`