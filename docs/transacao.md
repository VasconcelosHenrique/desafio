#Exibir transacao

Este recurso permite visualizar o dados de uma transação. Disponível apenas para usuários com acesso administrativo.

*URL** : `/transacao/{id}`

**Método** : `GET`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_ADMIN

**Exemplo** :

```
curl -X GET --header 'Accept: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjQxNzk3N30.tF9sWgEc5GCP8Iy85qQNFHlByK1hSftKJGCH8bZgEkCPdhdow4_Z6AEqyBFnz0ROsW4kEzFa_hFwqLfP3zCkZg' \
'http://localhost:8080/transacao/1'
```

#Sucesso

**Código** : `200 OK`

**Resposta [Body]** :

```json
{
    "id": 1,
    "valor": 100,
    "dataTransacao": "2018-09-09T20:46:10.024+0000",
    "conta": {
        "id": 1,
        "saldo": 3100,
        "limiteSaqueDiario": 1000,
        "ativo": true,
        "dataCriacao": "2018-09-09T20:46:05.443+0000",
        "tipoConta": "POUPANCA",
        "pessoa": {
            "id": 3,
            "nome": "João Ryco",
            "cpf": "741.801.439-26",
            "dataNascimento": "1968-08-10T00:00:00.000+0000"
        }
    }
}
```

#Excluir transacao

Este recurso permite excluir o dados de uma transação. Disponível apenas para usuários com acesso administrativo.

**URL** : `/transacao/{id}`

**Método** : `DELETE`

**Autenticação requerida** : Sim

**Nível de autorização** : ROLE_ADMIN

**Exemplo** :

```
curl -X DELETE --header 'Accept: application/json' --header \
'Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTUzNjQxNzk3N30.tF9sWgEc5GCP8Iy85qQNFHlByK1hSftKJGCH8bZgEkCPdhdow4_Z6AEqyBFnz0ROsW4kEzFa_hFwqLfP3zCkZg' \
'http://localhost:8080/transacao/1'
```

#Sucesso

**Código** : `204 OK`