# Api de gestão de contas
Implementação de uma API Rest de controle bancario, proposto como desafio para concorrer a vaga de desenvolvedor.

### Detalhes da API
A API contém as seguintes características:

Projeto criado com Spring Boot e Java 8 utilizando o SGBD MySQL com JPA e Spring Data JPA.
Controle de dependencias com Maven. Testes unitários e de integração com JUnit e Mockito.

## Como executar a aplicação
--------
### Pré-requisitos
* Apache Maven 3.5+
* Java JDK 8
* Git
* MySql 5.6+

Realize o clone do projeto:
```
$git clone  https://VasconcelosHenrique@bitbucket.org/VasconcelosHenrique/desafio.git
$cd desafio
```

Acesse e edite arquivo e edite o arquivo na pasta src/main/resources/application-dev.properties e altere as seguites propriedades para que
conbinem com seu banco instalado:
```
spring.datasource.url=jdbc:mysql://localhost:3306/controle_bancario //path do BD
spring.datasource.username=root                                     //usuário
spring.datasource.password=12345                                    //senha
```
Obs.: Essa configuração utiliza o driver do Mysql é possível utilizar outro banco apenas informando um novo driver

Logo após, crie um banco de dados com o mesmo nome informado no arquivo de configuracoes.

Após a conclusão das etapas anteriores, volte para a pasta raiz do projeto e execute o seguite comando:
```
$mvn spring-boot:run
```

Acesse a [documentação](docs) para ver a lista de recursos disponíveis. 
