insert into pessoa values(1, '123.456.789-10', '1996-11-02', 'Severino Heitor Teixeira');
insert into pessoa values(2, '496.511.510-48', '1984-01-15', 'Isis Betina Aragão');

insert into usuario values (1, '$2a$04$UashWYD1EI.D/6f84383pug8Gv8Po/ocstO30sYD4A44RQ8iHoRva', 'admin', null); /* usuario = admin, senha =  admin123 */
insert into usuario values (2, '$2a$04$geyfwEstyqZkb0cvUozVrOdDx.L8UoIf1qKcM2r1EWL0Jp0PcrzVS', 'severino', 1); /* usuario = severino, senha = severino */
insert into usuario values (3, '$2a$04$g15a4H0PP60JSDlSdY.Jnuop7hzmSPz5SqRRQtS8pXdNstyceSsqO', 'isis', 2); /* usuario = isis, senha = isis */

insert into usuario_roles values(1, 'ROLE_ADMIN');
insert into usuario_roles values(1, 'ROLE_USER');
insert into usuario_roles values(2, 'ROLE_USER');
insert into usuario_roles values(3, 'ROLE_USER');
