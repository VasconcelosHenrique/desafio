package com.vh.desafio.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;

public class JWTAuthenticationFailureHandler implements AuthenticationFailureHandler {

    /**
     * Intercepta e formata a mensagem de erro em caso de falha na autenticacao
     *
     * @param request
     * @param response
     * @param exception
     *
     * @throws IOException, ServletException
     * */
    @Override
    public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {

        response.setStatus(401);
        response.setContentType("application/json");
        response.getWriter().append(onFailureBodyMessage(exception));
    }

    /**
     * Define a mensagem de erro durante a falha de autenticacao
     *
     * @param exception
     * @return String
     * */
    private String onFailureBodyMessage(AuthenticationException exception) {
        long date = new Date().getTime();
        return "{\"timestamp\": " + date + ", "
                + "\"status\": 401, "
                + "\"error\": \"Não autorizado\", "
                + "\"message\": \"" + exception.getMessage() +"\", "
                + "\"path\": \"/login\"}";
    }
}
