package com.vh.desafio.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.vh.desafio.controllers.dto.CredenciaisDTO;
import com.vh.desafio.models.Usuario;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private AuthenticationManager authenticationManager;

    private JWTUtil jwtUtil;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, JWTUtil jwtUtil) {
        setAuthenticationFailureHandler(new JWTAuthenticationFailureHandler());
        this.authenticationManager = authenticationManager;
        this.jwtUtil = jwtUtil;
    }

    /**
     * Traduz as credenciais provenientes da requisicao para realizar uma tentatica de autenticacao
     *
     * @param request
     * @param response
     * @return Authentication
     *
     * @throws throws AuthenticationException
     * */
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request,
                                                HttpServletResponse response) throws AuthenticationException {

        UsernamePasswordAuthenticationToken authenticationToken;
        CredenciaisDTO credenciais;

        try {
            credenciais = new ObjectMapper().readValue(
                    request.getInputStream(),
                    CredenciaisDTO.class);

            authenticationToken = new UsernamePasswordAuthenticationToken(
                    credenciais.getUsername(),
                    credenciais.getPassword());

            Authentication authentication = authenticationManager.authenticate(authenticationToken);
            return authentication;

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Em caso de sucesso na autenticacao gera um token e anexa no header
     *
     * @param request
     * @param response
     * @param chain
     * @param authentication
     *
     * @throws IOException, ServletException
     * */
    @Override
    public void successfulAuthentication(HttpServletRequest request,
                                         HttpServletResponse response,
                                         FilterChain chain,
                                         Authentication authentication) throws IOException, ServletException {

        String username = ((Usuario) authentication.getPrincipal()).getUsername();
        String token = jwtUtil.generateToken(username);
        response.addHeader("Authorization", "Bearer " + token);
        response.setStatus(HttpStatus.NO_CONTENT.value());
    }

}