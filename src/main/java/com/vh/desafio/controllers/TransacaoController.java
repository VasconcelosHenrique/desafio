package com.vh.desafio.controllers;

import com.vh.desafio.models.Transacao;
import com.vh.desafio.services.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/transacao")
public class TransacaoController {

    @Autowired
    private TransacaoService transacaoService;

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    public ResponseEntity<Transacao> find(@PathVariable Integer id){

        Transacao transacao = transacaoService.find(id);

        return ResponseEntity.ok().body(transacao);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id){

        transacaoService.delete(id);

        return ResponseEntity.noContent().build();
    }
}
