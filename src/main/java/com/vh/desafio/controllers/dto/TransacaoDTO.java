package com.vh.desafio.controllers.dto;

import com.vh.desafio.models.Transacao;

import java.io.Serializable;
import java.util.Date;

public class TransacaoDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer id;

    private Double valor;

    private Date dataTransacao;

    public TransacaoDTO() {}

    public TransacaoDTO(Transacao transacao) {

        this.id            = transacao.getId();
        this.valor         = transacao.getValor();
        this.dataTransacao = transacao.getDataTransacao();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public Date getDataTransacao() {
        return dataTransacao;
    }

    public void setDataTransacao(Date dataTransacao) {
        this.dataTransacao = dataTransacao;
    }
}
