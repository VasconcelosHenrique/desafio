package com.vh.desafio.controllers;


import com.vh.desafio.controllers.dto.TransacaoDTO;
import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Transacao;
import com.vh.desafio.services.ContaService;
import com.vh.desafio.services.TransacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/conta")
public class ContaController {

    @Autowired
    private ContaService contaService;

    @Autowired
    private TransacaoService transacaoService;

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    public ResponseEntity<Conta> find(@PathVariable Integer id){

        Conta conta = contaService.find(id);

        return ResponseEntity.ok().body(conta);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<Conta> create(@Valid @RequestBody Conta conta){

        Conta contaCriada = contaService.create(conta);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(contaCriada.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @RequestMapping(value = "/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable Integer id,  @RequestBody Conta conta){

        contaService.update(id, conta);

        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id){

        contaService.delete(id);

        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "/{id}/saque")
    public ResponseEntity<Transacao> saque(
            @PathVariable Integer id,
            @RequestBody TransacaoDTO transacaoDTO){

        Transacao transacao = processarOperacao(id, "saque", transacaoDTO);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(transacao.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @PostMapping(value = "/{id}/deposito")
    public ResponseEntity<Transacao> deposito(
            @PathVariable Integer id,
            @RequestBody TransacaoDTO transacaoDTO){

        Transacao transacao = processarOperacao(id, "deposito", transacaoDTO);

        URI uri = ServletUriComponentsBuilder.fromCurrentServletMapping()
                .path("transacao/{id}").buildAndExpand(transacao.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @GetMapping(value = "/{id}/extrato")
    public ResponseEntity<Page<TransacaoDTO>> extrato(
            @PathVariable Integer id,
            @RequestParam(value = "dataInicial", required = true) String dataInicial,
            @RequestParam(value = "dataFinal", required = true) String dataFinal,
            @RequestParam(value = "page", defaultValue = "0") Integer page
            ){

        Page<Transacao> list       = transacaoService.extratoPorPeriodo(id, dataInicial, dataFinal, page);
        Page<TransacaoDTO> listDto = list.map(obj -> new TransacaoDTO(obj));

        return ResponseEntity.ok(listDto);
    }

    private Transacao processarOperacao(Integer id, String operacao, TransacaoDTO transacaoDTO){

        Double valor = Math.abs(transacaoDTO.getValor());

        if(operacao.equals("saque")){
            transacaoDTO.setValor(valor * (-1));
        }

        Transacao transacao = transacaoService.fromDTO(transacaoDTO);

        transacao.setConta(new Conta());
        transacao.getConta().setId(id);

        return transacaoService.create(transacao);
    }
}
