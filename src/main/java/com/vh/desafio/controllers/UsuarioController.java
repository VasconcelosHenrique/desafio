package com.vh.desafio.controllers;

import com.vh.desafio.models.Usuario;
import com.vh.desafio.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value="/{id}", method= RequestMethod.GET)
    public ResponseEntity<Usuario> find(@PathVariable Integer id){

        Usuario usuario = usuarioService.find(id);

        return ResponseEntity.ok().body(usuario);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @GetMapping(value = "/listar")
    public  ResponseEntity<List<Usuario>> findAll(){

        List<Usuario> usuarioList = usuarioService.list();

        return ResponseEntity.ok().body(usuarioList);
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @PostMapping
    public ResponseEntity<Usuario> create(@Valid @RequestBody Usuario usuario){

        Usuario usuarioCriado = usuarioService.create(usuario);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(usuarioCriado.getId()).toUri();

        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value = "/{id}", method=RequestMethod.PUT)
    public ResponseEntity<Void> update(@PathVariable Integer id,  @RequestBody Usuario usuario){

        usuarioService.update(id, usuario);

        return ResponseEntity.noContent().build();
    }

    @PreAuthorize("hasAnyRole('ADMIN')")
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable Integer id){

        usuarioService.delete(id);

        return ResponseEntity.noContent().build();
    }
}
