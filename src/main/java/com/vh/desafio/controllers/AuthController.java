package com.vh.desafio.controllers;

import com.vh.desafio.models.Usuario;
import com.vh.desafio.security.JWTUtil;
import com.vh.desafio.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;


@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    @Autowired
    private JWTUtil jwtUtil;

    @Autowired
    private UsuarioService usuarioService;

    @GetMapping(value = "/refresh_token")
    public ResponseEntity<Void> refreshToken(HttpServletResponse response) {

        Usuario ususario = usuarioService.authenticated();
        String token     = jwtUtil.generateToken(ususario.getUsername());

        response.addHeader("Authorization", "Bearer " + token);
        return ResponseEntity.noContent().build();
    }
}