package com.vh.desafio.services;

import com.vh.desafio.models.Usuario;
import com.vh.desafio.services.exceptions.AuthorizationException;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UsuarioService extends UserDetailsService {

    /**
     * Consulta os dados do registro de um usuario pelo id
     *
     * @param id
     * @return Usuario
     *
     * @throws AuthorizationException, ObjectNotFoundException
     * */
    Usuario find(Integer id) throws AuthorizationException, ObjectNotFoundException;

    /**
     * Lista os registros de usuario cadastrados no sistema
     *
     * @return List<Usuario>
     * */
    List<Usuario> list();

    /**
     * Cria um novo registro de usuario
     *
     * @param usuario
     * @return Usuario
     *
     * @throws DataIntegrityException
     * */
    Usuario create(Usuario usuario) throws DataIntegrityException;

    /**
     * Atualiza os dados de registro de um usuario
     *
     * @param id
     * @param usuario
     * @return Usuario
     * */
    Usuario update(Integer id, Usuario usuario) throws AuthorizationException;

    /**
     * Exclui o registro de um usuario
     *
     * @param id
     * */
    void delete(Integer id);

    /**
     * Consulta o usuario que esta autenticado
     *
     * @return Usuario
     * */
    Usuario authenticated();


}
