package com.vh.desafio.services;

import com.vh.desafio.models.Pessoa;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;

public interface PessoaService {

    /**
     * Consulta os dados do registro de uma pessoa pelo id
     *
     * @param id
     * @return Pessoa
     *
     * @throws ObjectNotFoundException
     * */
    Pessoa find(Integer id) throws ObjectNotFoundException;

    /**
     * Cria um novo registro de pessoa
     *
     * @param pessoa
     * @return Pessoa
     *
     * @throws DataIntegrityException
     * */
    Pessoa create(Pessoa pessoa) throws DataIntegrityException;

    /**
     * Atualiza os dados de registro de uma pessoa
     *
     * @param id
     * @param pessoa
     * @return Pessoa
     * */
    Pessoa update(Integer id, Pessoa pessoa);

    /**
     * Exclui o registro de uma pessoa
     *
     * @param id
     * */
    void delete(Integer id);

}
