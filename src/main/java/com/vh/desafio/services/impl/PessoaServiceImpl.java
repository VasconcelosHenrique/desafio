package com.vh.desafio.services.impl;

import com.vh.desafio.models.Pessoa;
import com.vh.desafio.models.Usuario;
import com.vh.desafio.models.enums.Role;
import com.vh.desafio.repositories.PessoaRepository;
import com.vh.desafio.services.PessoaService;
import com.vh.desafio.services.UsuarioService;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PessoaServiceImpl implements PessoaService {
    
    @Autowired
    private PessoaRepository pessoaRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public Pessoa find(Integer id) throws ObjectNotFoundException {

        Optional<Pessoa> pessoa;
        Usuario usuario = usuarioService.authenticated();

        if(usuario.hasRole(Role.ROLE_ADMIN)){
            pessoa = pessoaRepository.findById(id);
        } else {
            pessoa = pessoaRepository.findByIdAndUsuario(id, usuario);
        }

        return pessoa.orElseThrow(() -> new ObjectNotFoundException("Pessoa nao encontrada"));
    }

    @Override
    public Pessoa create(Pessoa pessoa) throws DataIntegrityException {

        Usuario usuario = pessoa.getUsuario();

        if(pessoaRepository.findByCpf(pessoa.getCpf()).isPresent()){
            throw new DataIntegrityException("Cpf ja utilizado");
        }

        if(usuario == null || usuario.getId() == null){
            throw new DataIntegrityException("Nenhum usuario foi informado");
        }

        pessoa.setId(null);
        pessoa.setUsuario(usuarioService.find(usuario.getId()));
        return pessoaRepository.save(pessoa);
    }

    @Override
    public Pessoa update(Integer id, Pessoa pessoa) {

        Pessoa pessoaAtual = find(id);
        updateData(pessoaAtual, pessoa);

        return pessoaRepository.save(pessoaAtual);
    }

    @Override
    public void delete(Integer id) {

        Pessoa pessoa = find(id);

        pessoaRepository.delete(pessoa);
    }

    private void updateData(Pessoa pessoaAtual, Pessoa pessoa) {

        Usuario usuario = pessoa.getUsuario();

        if(usuario != null && usuario.getId() != null){
            usuario = usuarioService.update(pessoaAtual.getUsuario().getId(), pessoa.getUsuario());
            pessoa.setUsuario(usuario);
        }

        pessoaAtual.setNome(pessoa.getNome());
    }
}
