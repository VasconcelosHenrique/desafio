package com.vh.desafio.services.impl;

import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Pessoa;
import com.vh.desafio.models.Usuario;
import com.vh.desafio.models.enums.Role;
import com.vh.desafio.repositories.ContaRepository;
import com.vh.desafio.services.ContaService;
import com.vh.desafio.services.PessoaService;
import com.vh.desafio.services.UsuarioService;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContaServiceImpl implements ContaService {

    @Autowired
    private ContaRepository contaRepository;

    @Autowired
    private PessoaService pessoaService;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public Conta find(Integer id) throws ObjectNotFoundException {

        Optional<Conta> conta;
        Usuario usuario = usuarioService.authenticated();

        if(usuario.hasRole(Role.ROLE_ADMIN)) {
            conta = contaRepository.findById(id);
        }else{
            conta = contaRepository.findByIdAndPessoaUsuario(id, usuario);
        }

        return conta.orElseThrow(() -> new ObjectNotFoundException("Conta nao encontrada"));
    }

    @Override
    public Conta create(Conta conta) throws DataIntegrityException{

        Pessoa pessoa = conta.getPessoa();

        if(pessoa == null){
            throw new DataIntegrityException("Nenhuma pessoa foi informada");
        }

        Integer pessoaId = conta.getPessoa().getId();
        pessoa = (pessoaId == null) ?
            pessoaService.create(conta.getPessoa()) :
            pessoaService.find(pessoaId);

        conta.setId(null);
        conta.setPessoa(pessoa);

        return contaRepository.save(conta);
    }

    @Override
    public Conta update(Integer id, Conta conta){

        Conta contaAtual = find(id);
        updateData(contaAtual, conta);

        return contaRepository.save(conta);
    }

    @Override
    public void delete(Integer id){

        Conta conta = find(id);

        contaRepository.delete(conta);
    }

    @Override
    public Double saldoLimiteSaqueDiario(Conta conta) {

        Double valor = contaRepository.findSaquesHoje(conta);

        return conta.getLimiteSaqueDiario() - ((valor == null) ? 0 : Math.abs(valor));
    }

    private void updateData(Conta contaAtual, Conta conta) {

        contaAtual.setLimiteSaqueDiario(conta.getLimiteSaqueDiario());
        contaAtual.setAtivo(conta.getAtivo());
    }
}
