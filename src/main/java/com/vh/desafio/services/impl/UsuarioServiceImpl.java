package com.vh.desafio.services.impl;

import com.vh.desafio.models.Usuario;
import com.vh.desafio.models.enums.Role;
import com.vh.desafio.repositories.UsuarioRepository;
import com.vh.desafio.services.UsuarioService;
import com.vh.desafio.services.exceptions.AuthorizationException;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private PasswordEncoder pe;

    @Override
    public Usuario find(Integer id) throws AuthorizationException, ObjectNotFoundException {

        Usuario usuarioAutenticado = authenticated();
        if (id.equals(usuarioAutenticado.getId())) {
            return usuarioAutenticado;
        }

        if(usuarioAutenticado.hasRole(Role.ROLE_ADMIN)){
            Optional<Usuario> usuario = usuarioRepository.findById(id);
            return usuario.orElseThrow(() -> new ObjectNotFoundException("Usuario nao encontrado"));
        }

        throw new AuthorizationException("Denied Access");
    }

    @Override
    public List<Usuario> list(){

        return usuarioRepository.findAll();
    }

    @Override
    public Usuario create(Usuario usuario) throws DataIntegrityException{

        if (usuarioRepository.findByUsername(usuario.getUsername()).isPresent()){
            throw new DataIntegrityException("Username ja utilizado");
        }

        usuario.setId(null);
        usuario.grantAuthority(Role.ROLE_USER);
        usuario.setPassword(pe.encode(usuario.getPassword()));

        return usuarioRepository.save(usuario);
    }

    @Override
    public Usuario update(Integer id, Usuario usuario) throws AuthorizationException {

        Usuario atual = find(id);
        updateData(atual, usuario);

        return usuarioRepository.save(atual);
    }

    @Override
    public void delete(Integer id) throws DataIntegrityException {

        Usuario usuario = find(id);

        if(usuario.getPessoa() != null){
            throw new DataIntegrityException("Nao e possível excluir um usuario vinculado a uma pessoa");
        }

        usuarioRepository.delete(usuario);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        Optional<Usuario> usuario = usuarioRepository.findByUsername(username);

        if(!usuario.isPresent()){
            throw new UsernameNotFoundException("Usuario nao encontrado");
        }

        return usuario.get();
    }

    @Override
    public Usuario authenticated() {

        return (Usuario) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    private void updateData(Usuario usuarioAtual, Usuario usuario) throws DataIntegrityException {

        if(usuarioAtual.getUsername() != usuario.getUsername()){
            Optional<Usuario> usuarioRetornado = usuarioRepository.findByUsername(usuario.getUsername());

            if(usuarioRetornado.isPresent() && !usuarioRetornado.equals(usuarioAtual)){
                throw new DataIntegrityException("Username ja utilizado");
            }
        }

        usuarioAtual.setUsername(usuario.getUsername());
        usuarioAtual.setPassword(pe.encode(usuario.getPassword()));

        if(authenticated().hasRole(Role.ROLE_ADMIN)){
            usuario.getRoles().forEach(role -> usuarioAtual.grantAuthority(role));
        }
    }
}
