package com.vh.desafio.services.impl;

import com.vh.desafio.controllers.dto.TransacaoDTO;
import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Transacao;
import com.vh.desafio.models.Usuario;
import com.vh.desafio.models.enums.Role;
import com.vh.desafio.repositories.TransacaoRepository;
import com.vh.desafio.services.ContaService;
import com.vh.desafio.services.TransacaoService;
import com.vh.desafio.services.UsuarioService;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.InvalidFormatException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Service
public class TransacaoServiceImpl implements TransacaoService {

    @Autowired
    private TransacaoRepository transacaoRepository;

    @Autowired
    private ContaService contaService;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public Transacao find(Integer id) throws ObjectNotFoundException {

        Optional<Transacao> transacao;

        Usuario usuarioAutenticado = usuarioService.authenticated();
        if(usuarioAutenticado.hasRole(Role.ROLE_ADMIN)){
            transacao = transacaoRepository.findById(id);
        }else {
            transacao = transacaoRepository.findByIdAndContaPessoaUsuario(id, usuarioAutenticado);
        }

        return transacao.orElseThrow(() -> new ObjectNotFoundException("Transacao nao encontrada"));
    }

    @Override
    @Transactional
    public Transacao create(Transacao transacao) throws DataIntegrityException{

        if(transacao.getConta() == null || transacao.getConta().getId() == null){
            throw new DataIntegrityException("Nenhuma conta foi informada");
        }

        Conta conta = contaService.find(transacao.getConta().getId());

        if(transacao.getValor() < 0 && Math.abs(transacao.getValor()) > contaService.saldoLimiteSaqueDiario(conta)){
            throw new DataIntegrityException("Valor da transacao excede o limite diario para saque");
        }

        transacao.setId(null);
        transacao.setConta(conta);
        conta.addTransacao(transacao);

        return transacaoRepository.save(transacao);
    }

    @Override
    public void delete(Integer id) {

        Transacao transacao = find(id);

        transacaoRepository.delete(transacao);
    }

    @Override
    public Page<Transacao> extratoPorPeriodo(Integer id, String dataInicial, String dataFinal, Integer page) throws InvalidFormatException {

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Date dataIni;
        Date dataFim;

        try {
            dataIni = formatter.parse(dataInicial);
            dataFim = formatter.parse(dataFinal);
        } catch (ParseException e) {
            throw new InvalidFormatException("Formatação invalida. Use yyyy-MM-dd");
        }

        PageRequest pageRequest = PageRequest.of(page, 20, Sort.Direction.DESC, "dataTransacao");
        Conta conta             = contaService.find(id);

        return transacaoRepository.findAllByContaAndDataTransacaoBetween(conta, dataIni, dataFim, pageRequest);
    }

    @Override
    public Transacao fromDTO(TransacaoDTO transacao){

        return new Transacao(transacao.getValor(), transacao.getDataTransacao(), null);
    }

}
