package com.vh.desafio.services;

import com.vh.desafio.models.Conta;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;

public interface ContaService {

    /**
     * Consulta os dados do registro de uma conta pelo id
     *
     * @param id
     * @return Conta
     *
     * @throws ObjectNotFoundException
     * */
    Conta find(Integer id) throws ObjectNotFoundException;

    /**
     * Cria um novo registro de conta
     *
     * @param conta
     * @return Conta
     *
     * @throws DataIntegrityException
     * */
    Conta create(Conta conta) throws DataIntegrityException;

    /**
     * Atualiza os dados de registro de uma conta
     *
     * @param id
     * @param conta
     * @return Conta
     * */
    Conta update(Integer id, Conta conta);

    /**
     * Exclui o registro de uma conta
     *
     * @param id
     * */
    void delete(Integer id);

    /**
     * Consulta o saldo de limite de saque diario
     *
     * @param conta
     * @return Double
     * */
    Double saldoLimiteSaqueDiario(Conta conta);
}
