package com.vh.desafio.services.exceptions;

public class InvalidFormatException extends RuntimeException{
	
	private static final long serialVersionUID = 1L;

	public InvalidFormatException(String message) {
		super(message);
	}

	public InvalidFormatException(String message, Throwable cause) {
		super(message, cause);
	}
	
	
}
