package com.vh.desafio.services;

import com.vh.desafio.controllers.dto.TransacaoDTO;
import com.vh.desafio.models.Transacao;
import com.vh.desafio.services.exceptions.AuthorizationException;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.InvalidFormatException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.springframework.data.domain.Page;

public interface TransacaoService {

    /**
     * Consulta os dados do registro de um transacao pelo id
     *
     * @param id
     * @return Transacao
     *
     * @throws ObjectNotFoundException
     * */
    Transacao find(Integer id) throws ObjectNotFoundException;

    /**
     * Cria um novo registro de transacao
     *
     * @param transacao
     * @return Transacao
     *
     * @throws DataIntegrityException
     * */
    Transacao create(Transacao transacao) throws DataIntegrityException;

    /**
     * Exclui o registro de um transacao
     *
     * @param id
     * */
    void delete(Integer id);

    /**
     * Consulta o historico de transacoes de uma conta por periodo informado
     *
     * @param id            Id da conta
     * @param dataInicial   Data inicial do intevalo
     * @param dataFinal     Data final do intervalo
     * @param page          Numero da pagina caso haja mais de uma
     * @return Page<Transacao>
     * */
    Page<Transacao> extratoPorPeriodo(Integer id, String dataInicial, String dataFinal, Integer page) throws InvalidFormatException;

    /**
     * Transforma um objeto TransacaoDTO em um objeto Transacao
     *
     * @param transacaoDTO
     * @return Transacao
     * */
    Transacao fromDTO(TransacaoDTO transacaoDTO);
}
