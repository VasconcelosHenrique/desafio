package com.vh.desafio.repositories;

import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Transacao;
import com.vh.desafio.models.Usuario;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.Optional;

@Repository
public interface TransacaoRepository extends JpaRepository<Transacao, Integer> {

    Optional<Transacao> findByIdAndContaPessoaUsuario(Integer id, Usuario usuario);

    @Transactional(readOnly=true)
    Page<Transacao> findAllByContaAndDataTransacaoBetween(Conta conta, Date dataInicial, Date dataFinal, Pageable pageRequest);


}
