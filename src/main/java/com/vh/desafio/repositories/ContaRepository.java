package com.vh.desafio.repositories;

import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreFilter;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.Optional;

@Repository
public interface ContaRepository extends JpaRepository<Conta, Integer> {

    Optional<Conta> findByIdAndPessoaUsuario(Integer id, Usuario usuario);

    @Query(
            "SELECT SUM(t.valor) FROM Transacao t " +
            "WHERE t.conta = :conta AND t.dataTransacao LIKE CONCAT(CURRENT_DATE, '%') AND t.valor < 0")
    Double findSaquesHoje(@Param("conta") Conta conta);
}
