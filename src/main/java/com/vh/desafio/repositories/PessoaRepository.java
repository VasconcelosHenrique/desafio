package com.vh.desafio.repositories;

import com.vh.desafio.models.Pessoa;
import com.vh.desafio.models.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PessoaRepository extends JpaRepository<Pessoa, Integer> {

    Optional<Pessoa> findByCpf(String cpf);

    Optional<Pessoa> findByIdAndUsuario(Integer id, Usuario usuario);
}
