package com.vh.desafio.models;

import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.vh.desafio.models.enums.TipoConta;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;

@Entity
@Where(clause = "ativo = true")
public class Conta implements Observer, Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    private Double saldo;

    @NotNull
    private Double limiteSaqueDiario;

    private Boolean ativo;

    private Date dataCriacao;

    @NotNull
    @JsonEnumDefaultValue
    @Enumerated(EnumType.ORDINAL)
    private TipoConta tipoConta;

    @ManyToOne
    @JoinColumn(name = "pessoa_id")
    private Pessoa pessoa;

    @JsonIgnore
    @OneToMany(mappedBy = "conta", fetch = FetchType.LAZY)
    private List<Transacao> transacoes;

    public Conta() {}

    public Conta(
            Double saldo,
            Double limiteSaqueDiario,
            Boolean ativo,
            Date dataCriacao,
            TipoConta tipoConta,
            Pessoa pessoa) {

        this.saldo             = saldo;
        this.limiteSaqueDiario = limiteSaqueDiario;
        this.ativo             = ativo;
        this.dataCriacao       = dataCriacao;
        this.tipoConta         = tipoConta;
        this.pessoa            = pessoa;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getSaldo() {
        return saldo;
    }

    public void setSaldo(Double saldo) {
        this.saldo = saldo;
    }

    public Double getLimiteSaqueDiario() {
        return limiteSaqueDiario;
    }

    public void setLimiteSaqueDiario(Double limiteSaqueDiario) {
        this.limiteSaqueDiario = limiteSaqueDiario;
    }

    public Boolean getAtivo() {
        return ativo;
    }

    public void setAtivo(Boolean ativo) {
        this.ativo = ativo;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public TipoConta getTipoConta() {
        return tipoConta;
    }

    public void setTipoConta(TipoConta tipoConta) {
        this.tipoConta = tipoConta;
    }

    public Pessoa getPessoa() {
        return pessoa;
    }

    public void setPessoa(Pessoa pessoa) {
        this.pessoa = pessoa;
    }

    public List<Transacao> getTransacoes() {
        return transacoes;
    }

    public void addTransacao(Transacao transacao) {

        if(this.transacoes == null){
            this.transacoes = new ArrayList<Transacao>();
        }

        transacao.addObserver(this);
        this.transacoes.add(transacao);
    }

    /**
     * Atualiza o saldo de acordo com as transações incluídas ou removidas da conta
     *
     * @param obj Transação
     * @param arg Ação
     * */
    @Override
    public void update(Observable obj, Object arg) {

        String acao = String.valueOf(arg);

        switch (acao){
            case "create":
                this.saldo += ((Transacao) obj).getValor();

                if(this.saldo < 0){
                    throw new DataIntegrityException("Saldo insuficiente");
                }

                break;
            case "remove":
                this.saldo -= ((Transacao) obj).getValor();
                break;
        }
    }

    @PrePersist
    public void prePersist(){

        this.dataCriacao = new Date();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Conta conta = (Conta) o;
        return Objects.equals(id, conta.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
