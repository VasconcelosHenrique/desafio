package com.vh.desafio.models.enums;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum Role {

    @JsonProperty("ROLE_ADMIN")
    ROLE_ADMIN,
    @JsonProperty("ROLE_USER")
    ROLE_USER;
}
