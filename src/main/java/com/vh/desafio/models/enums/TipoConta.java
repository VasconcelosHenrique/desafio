package com.vh.desafio.models.enums;

public enum TipoConta {

    CORRENTE,
    POUPANCA;
}
