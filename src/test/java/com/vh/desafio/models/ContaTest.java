package com.vh.desafio.models;

import com.vh.desafio.data.TransacaoBuilder;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContaTest {

    @Rule
    public ExpectedException exception;

    private Transacao transacao;
    private Conta conta;

    private static final Double SALDO               = 1000.00;

    public ContaTest() {

        exception = ExpectedException.none();
    }

    @Before
    public void setUp(){

        transacao = TransacaoBuilder.create().comConta().get();
        conta     = transacao.getConta();
    }

    @Test
    public void contaDeveAtualizarSaldoQuandoTransacaoForPersistida(){

        this.conta.setSaldo(SALDO);
        this.transacao.setValor(100.00);

        this.transacao.prePersist();

        Double expected = SALDO + transacao.getValor();
        assertEquals(expected, this.conta.getSaldo());
    }

    @Test
    public void contaDeveLancarExcecaoQuandoNaoPossuirSaldoAoPersistirTransacao(){

        this.conta.setSaldo(SALDO);
        this.transacao.setValor(1001.00 * (-1));

        exception.expect(DataIntegrityException.class);
        exception.expectMessage("Saldo insuficiente");

        this.transacao.prePersist();
    }
}
