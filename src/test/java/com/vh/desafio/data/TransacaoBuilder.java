package com.vh.desafio.data;

import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Transacao;

import java.util.Date;

public class TransacaoBuilder {

    private Transacao transacao;

    private TransacaoBuilder(){}

    public static TransacaoBuilder create(){

        TransacaoBuilder transacaoBuilder = new TransacaoBuilder();
        transacaoBuilder.transacao        = new Transacao();

        return transacaoBuilder;
    }

    public TransacaoBuilder comConta(){

        Conta conta = ContaBuilder.create().comPessoa().get();
        conta.addTransacao(this.transacao);
        this.transacao.setConta(conta);

        return this;
    }

    public Transacao get(){

        return this.transacao;
    }
}
