package com.vh.desafio.data;

import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Pessoa;

public class ContaBuilder {

    private Conta conta;

    private ContaBuilder(){}

    public static ContaBuilder create(){

        ContaBuilder contaBuilder = new ContaBuilder();
        contaBuilder.conta = new Conta();

        return contaBuilder;
    }

    public static ContaBuilder clone(Conta conta){

        ContaBuilder contaBuilder = new ContaBuilder();
        contaBuilder.conta = new Conta();

        contaBuilder.conta.setId(conta.getId());
        contaBuilder.conta.setSaldo(conta.getSaldo());
        contaBuilder.conta.setLimiteSaqueDiario(conta.getLimiteSaqueDiario());
        contaBuilder.conta.setTipoConta(conta.getTipoConta());
        contaBuilder.conta.setAtivo(conta.getAtivo());
        contaBuilder.conta.setDataCriacao(conta.getDataCriacao());
        contaBuilder.conta.setPessoa(conta.getPessoa());

        return contaBuilder;
    }

    public ContaBuilder comPessoa(){

        Pessoa pessoa = PessoaBuilder.create().comUsuario().get();

        pessoa.addConta(this.conta);
        conta.setPessoa(pessoa);

        return this;
    }

    public Conta get(){

        return this.conta;
    }
}
