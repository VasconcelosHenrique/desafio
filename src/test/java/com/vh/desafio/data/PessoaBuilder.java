package com.vh.desafio.data;

import com.vh.desafio.models.Pessoa;
import com.vh.desafio.models.Usuario;

public class PessoaBuilder {

    private Pessoa pessoa;

    private PessoaBuilder(){}

    public static PessoaBuilder create(){

        PessoaBuilder pessoaBuilder = new PessoaBuilder();
        pessoaBuilder.pessoa = new Pessoa();

        return pessoaBuilder;
    }

    public static PessoaBuilder clone(Pessoa pessoa){

        PessoaBuilder pessoaBuilder = new PessoaBuilder();
        pessoaBuilder.pessoa = new Pessoa();

        pessoaBuilder.pessoa.setId(pessoa.getId());
        pessoaBuilder.pessoa.setCpf(pessoa.getCpf());
        pessoaBuilder.pessoa.setNome(pessoa.getNome());
        pessoaBuilder.pessoa.setDataNascimento(pessoa.getDataNascimento());
        pessoaBuilder.pessoa.setUsuario(pessoa.getUsuario());

        return pessoaBuilder;
    }

    public PessoaBuilder comUsuario(){

        Usuario usuario = UsuarioBuilder.create().get();

        usuario.setPessoa(this.pessoa);
        this.pessoa.setUsuario(usuario);

        return this;
    }

    public Pessoa get(){

        return this.pessoa;
    }


}
