package com.vh.desafio.data;

import com.vh.desafio.models.Usuario;

public class UsuarioBuilder {

    private Usuario usuario;

    private UsuarioBuilder(){}

    public static UsuarioBuilder create(){

        UsuarioBuilder usuarioBuilder = new UsuarioBuilder();
        usuarioBuilder.usuario        = new Usuario();

        return usuarioBuilder;
    }

    public static UsuarioBuilder clone(Usuario usuario){

        UsuarioBuilder usuarioBuilder = new UsuarioBuilder();
        usuarioBuilder.usuario        = new Usuario();

        usuarioBuilder.usuario.setId(usuario.getId());
        usuarioBuilder.usuario.setUsername(usuario.getUsername());
        usuarioBuilder.usuario.setPassword(usuario.getPassword());
        usuarioBuilder.usuario.setPessoa(usuario.getPessoa());

        return usuarioBuilder;
    }

    public Usuario get(){

        return this.usuario;
    }
}
