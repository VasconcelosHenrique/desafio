package com.vh.desafio.data.util;

import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public final class RandomUtil {

    private static final SecureRandom RAND = new SecureRandom();

    public static <T extends Enum<?>> T randomEnum(Class<T> type) {

        List<T> values  = Collections.unmodifiableList(Arrays.asList(type.getEnumConstants()));
        Integer size    = values.size();

        return values.get(RAND.nextInt(size));
    }
}
