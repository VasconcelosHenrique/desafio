package com.vh.desafio.services;

import com.vh.desafio.data.UsuarioBuilder;
import com.vh.desafio.models.Usuario;
import com.vh.desafio.models.enums.Role;
import com.vh.desafio.repositories.UsuarioRepository;
import com.vh.desafio.services.exceptions.AuthorizationException;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UsuarioServiceTest {

    @SpyBean
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @MockBean
    private PasswordEncoder passwordEncoder;

    @Rule
    public ExpectedException exception;

    private Usuario usuario;

    private static Authentication authentication;

    private static SecurityContext securityContext;

    private static final Integer ID      = 10;
    private static final String USERNAME = "usuario";
    private static final String PASSWORD = "testepass";

    public UsuarioServiceTest(){

        exception       = ExpectedException.none();
        authentication  = mock(Authentication.class);
        securityContext = mock(SecurityContext.class);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Before
    public void setUp(){

        usuario = UsuarioBuilder.create().get();
        usuario.setId(ID);
        usuario.grantAuthority(Role.ROLE_USER);

        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(this.usuario);
    }

    @Test
    public void findUsuarioComMesmoIdQueLogado(){

        Usuario atual = usuarioService.find(ID);

        assertEquals(this.usuario, atual);
        verify(usuarioService, times(1)).authenticated();
        verifyZeroInteractions(usuarioRepository);
    }

    @Test
    public void findOutroUsuarioComAdmLogado(){

        this.usuario.grantAuthority(Role.ROLE_ADMIN);

        Usuario outroUsuario = UsuarioBuilder.create().get();
        outroUsuario.setId(2);

        when(usuarioRepository.findById(anyInt())).thenReturn(Optional.of(outroUsuario));

        usuarioService.find(outroUsuario.getId());

        verify(usuarioRepository, times(1)).findById(outroUsuario.getId());
    }

    @Test
    public void findOutroUsuarioSemPermissaoDeveLancarExcecao(){

        this.usuario.grantAuthority(Role.ROLE_USER);

        exception.expect(AuthorizationException.class);
        exception.expectMessage("Denied Access");

        usuarioService.find(1);
    }

    @Test
    public void create(){

        Usuario outroUsuario = UsuarioBuilder.create().get();
        outroUsuario.setUsername(USERNAME);
        outroUsuario.setPassword(PASSWORD);

        when(usuarioRepository.findByUsername(USERNAME)).thenReturn(Optional.empty());
        when(usuarioRepository.save(any(Usuario.class))).thenReturn(outroUsuario);

        usuarioService.create(outroUsuario);

        verify(usuarioRepository, times(1)).findByUsername(anyString());
        verify(passwordEncoder, times(1)).encode(anyString());
        verify(usuarioRepository, times(1)).save(outroUsuario);
    }

    @Test
    public void createUsuarioComUsernameExistenteDeveLancarExcecao(){

        Usuario outroUsuario = UsuarioBuilder.create().get();
        outroUsuario.setUsername(USERNAME);

        when(usuarioRepository.findByUsername(anyString())).thenReturn(Optional.of(this.usuario));

        exception.expect(DataIntegrityException.class);
        exception.expectMessage("Username ja utilizado");

        usuarioService.create(outroUsuario);
    }

    @Test
    public void update(){

        Usuario usuarioAtt = UsuarioBuilder.clone(this.usuario).get();
        usuarioAtt.setUsername(USERNAME);
        usuarioAtt.setPassword(PASSWORD);

        when(usuarioRepository.save(any(Usuario.class))).thenReturn(usuarioAtt);

        Usuario retornado = usuarioService.update(this.usuario.getId(), usuarioAtt);

        assertEquals(usuarioAtt, retornado);
        verify(usuarioRepository, times(1)).findByUsername(anyString());
        verify(passwordEncoder, times(1)).encode(anyString());
        verify(usuarioRepository, times(1)).save(usuarioAtt);
    }

    @Test
    public void updateUsuarioComUsernameExistenteDeveLancarExcecao(){

        Usuario usuarioAtt   = UsuarioBuilder.clone(this.usuario).get();
        Usuario outroUsuario = UsuarioBuilder.create().get();

        usuarioAtt.setUsername(USERNAME);

        when(usuarioRepository.findByUsername(anyString())).thenReturn(Optional.of(outroUsuario));

        exception.expect(DataIntegrityException.class);
        exception.expectMessage("Username ja utilizado");

        usuarioService.update(this.usuario.getId(), usuarioAtt);
    }

    @Test
    public void delete(){

        this.usuario.grantAuthority(Role.ROLE_ADMIN);

        Usuario outroUsuario = UsuarioBuilder.create().get();
        outroUsuario.setId(5);

        when(usuarioRepository.findById(anyInt())).thenReturn(Optional.of(outroUsuario));
        doNothing().when(usuarioRepository).delete(any(Usuario.class));

        usuarioService.delete(outroUsuario.getId());

        verify(usuarioService, times(1)).find(5);
        verify(usuarioRepository, times(1)).delete(outroUsuario);
    }
}
