package com.vh.desafio.services;

import com.vh.desafio.data.PessoaBuilder;
import com.vh.desafio.models.Pessoa;
import com.vh.desafio.models.Usuario;
import com.vh.desafio.models.enums.Role;
import com.vh.desafio.repositories.PessoaRepository;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PessoaServiceTest {

    @SpyBean
    private PessoaService pessoaService;

    @SpyBean
    private UsuarioService usuarioService;

    @MockBean
    private PessoaRepository pessoaRepository;

    @Rule
    public ExpectedException exception;

    private Pessoa pessoa;

    private Usuario usuario;

    private static Authentication authentication;

    private static SecurityContext securityContext;

    private static final Integer ID           = 11;
    private static final Integer USUARIO_ID   = 10;
    private static final String NOME          = "Nome Sobrenome";
    private static final String CPF           = "123.456.789-10";

    public PessoaServiceTest(){

        exception       = ExpectedException.none();
        authentication  = mock(Authentication.class);
        securityContext = mock(SecurityContext.class);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Before
    public void setUp(){

        pessoa  = PessoaBuilder.create().comUsuario().get();
        usuario = this.pessoa.getUsuario();

        pessoa.setId(ID);
        usuario.setId(USUARIO_ID);
        usuario.grantAuthority(Role.ROLE_USER);

        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(usuario);
    }

    @Test
    public void findPessoaQueRefenciaOMesmoUsuarioLogado(){

        when(pessoaRepository.findByIdAndUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(this.pessoa));

        Pessoa atual = pessoaService.find(ID);

        assertEquals(this.pessoa, atual);
        verify(usuarioService, times(1)).authenticated();
        verify(pessoaRepository).findByIdAndUsuario(ID, this.usuario);
        verifyNoMoreInteractions(pessoaRepository);
    }

    @Test
    public void findOutraPessoaSemPermissaoDeveRetornarExcecao(){

        exception.expect(ObjectNotFoundException.class);
        exception.expectMessage("Pessoa nao encontrada");

        pessoaService.find(1);
    }

    @Test
    public void findOutraPessoaComAdmLogado(){

        this.usuario.grantAuthority(Role.ROLE_ADMIN);

        Pessoa outraPessoa = PessoaBuilder.create().get();
        outraPessoa.setId(2);

        when(pessoaRepository.findById(anyInt())).thenReturn(Optional.of(outraPessoa));

        Pessoa retornada = pessoaService.find(2);

        assertEquals(outraPessoa, retornada);
        verify(usuarioService, times(1)).authenticated();
        verify(pessoaRepository, times(1)).findById(2);
        verifyNoMoreInteractions(pessoaRepository);
    }

    @Test
    public void create(){

        this.pessoa.setCpf(CPF);

        when(pessoaRepository.findByCpf(anyString())).thenReturn(Optional.empty());

        pessoaService.create(this.pessoa);

        verify(pessoaRepository, times(1)).findByCpf(anyString());
        verify(pessoaRepository, times(1)).save(this.pessoa);
    }

    @Test
    public void createPessoaComCpfExistenteDeveLancarExcecao(){

        Pessoa outraPessoa = PessoaBuilder.create().get();
        outraPessoa.setId(2);
        outraPessoa.setCpf(CPF);

        when(pessoaRepository.findByCpf(anyString())).thenReturn(Optional.of(this.pessoa));

        exception.expect(DataIntegrityException.class);
        exception.expectMessage("Cpf ja utilizado");

        pessoaService.create(outraPessoa);
    }

    @Test
    public void update(){

        Pessoa novaPessoa = PessoaBuilder.clone(this.pessoa).get();
        novaPessoa.setNome(NOME);
        novaPessoa.setUsuario(null);

        when(pessoaRepository.findByIdAndUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(this.pessoa));
        when(pessoaRepository.save(any(Pessoa.class))).thenReturn(novaPessoa);

        Pessoa retornada = pessoaService.update(this.pessoa.getId(), novaPessoa);

        assertEquals(novaPessoa, retornada);
        verify(pessoaRepository, times(1)).save(novaPessoa);
    }

    @Test
    public void delete(){

        this.usuario.grantAuthority(Role.ROLE_ADMIN);

        Pessoa outraPessoa = PessoaBuilder.create().get();
        outraPessoa.setId(5);

        when(pessoaRepository.findById(anyInt())).thenReturn(Optional.of(outraPessoa));
        doNothing().when(pessoaRepository).delete(any(Pessoa.class));

        pessoaService.delete(outraPessoa.getId());

        verify(pessoaService, times(1)).find(5);
        verify(pessoaRepository, times(1)).delete(outraPessoa);
    }
}
