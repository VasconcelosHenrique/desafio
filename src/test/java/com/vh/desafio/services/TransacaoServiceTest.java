package com.vh.desafio.services;

import com.vh.desafio.data.ContaBuilder;
import com.vh.desafio.data.TransacaoBuilder;
import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Transacao;
import com.vh.desafio.models.Usuario;
import com.vh.desafio.models.enums.Role;
import com.vh.desafio.repositories.ContaRepository;
import com.vh.desafio.repositories.TransacaoRepository;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TransacaoServiceTest {

    @SpyBean
    private TransacaoService transacaoService;

    @SpyBean
    private ContaServiceTest contaServiceTest;

    @SpyBean
    private UsuarioService usuarioService;

    @MockBean
    private TransacaoRepository transacaoRepository;

    @MockBean
    private ContaRepository contaRepository;

    @Rule
    public ExpectedException exception;

    private Conta conta;

    private Usuario usuario;

    private Transacao transacao;

    private static Authentication authentication;

    private static SecurityContext securityContext;


    private static final Integer ID                 = 1;
    private static final Integer CONTA_ID           = 2;
    private static final Integer USUARIO_ID         = 10;
    private static final Double SALDO               = 1000.00;
    private static final Double LIMITE_SAQUE_DIARIO = 1500.00;

    public TransacaoServiceTest(){

        exception       = ExpectedException.none();
        authentication  = mock(Authentication.class);
        securityContext = mock(SecurityContext.class);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Before
    public void setUp(){

        transacao = TransacaoBuilder.create().comConta().get();
        conta     = transacao.getConta();
        usuario   = conta.getPessoa().getUsuario();

        transacao.setId(ID);
        conta.setId(CONTA_ID);
        usuario.setId(USUARIO_ID);
        usuario.grantAuthority(Role.ROLE_USER);

        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(usuario);
    }

    @Test
    public void findTransacaoQuePertenceAoUsuarioLogado(){

        when(transacaoRepository.findByIdAndContaPessoaUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(this.transacao));

        Transacao retornada = transacaoService.find(ID);

        assertEquals(this.transacao, retornada);
        verify(transacaoRepository, times(1)).findByIdAndContaPessoaUsuario(ID, usuario);
        verify(usuarioService, times(1)).authenticated();
        verifyNoMoreInteractions(transacaoRepository);
    }

    @Test
    public void findOutraTransacaoSemPermissaoDeveLancarExcecao(){

        exception.expect(ObjectNotFoundException.class);
        exception.expectMessage("Transacao nao encontrada");

        transacaoService.find(2);
    }

    @Test
    public void findOutraTransacaoComAdmLogado(){

        this.usuario.grantAuthority(Role.ROLE_ADMIN);

        Transacao outraTransacao = TransacaoBuilder.create().get();
        outraTransacao.setId(2);

        when(transacaoRepository.findById(anyInt())).thenReturn(Optional.of(outraTransacao));

        Transacao retornada = transacaoService.find(outraTransacao.getId());

        assertEquals(outraTransacao, retornada);
        verify(transacaoRepository, times(1)).findById(outraTransacao.getId());
        verify(usuarioService, times(1)).authenticated();
        verifyNoMoreInteractions(transacaoRepository);
    }

    @Test
    public void create(){

        Transacao transacaoDeposito = TransacaoBuilder.create().get();

        transacaoDeposito.setValor(100.00);
        transacaoDeposito.setConta(this.conta);
        this.conta.setSaldo(SALDO);


        when(contaRepository.findByIdAndPessoaUsuario(CONTA_ID, this.usuario)).thenReturn(Optional.of(this.conta));
        when(contaRepository.save(any(Conta.class))).thenReturn(transacaoDeposito.getConta());
        when(transacaoRepository.save(any(Transacao.class))).thenReturn(transacaoDeposito);

        transacaoService.create(transacaoDeposito);

        verify(contaRepository, times(1)).findByIdAndPessoaUsuario(CONTA_ID, this.usuario);
        verify(transacaoRepository, times(1)).save(transacaoDeposito);
    }

    @Test
    public void createSemContaDeveLancarExcecao(){

        Transacao novaTransacao = TransacaoBuilder.create().get();

        exception.expect(DataIntegrityException.class);
        exception.expectMessage("Nenhuma conta foi informada");

        transacaoService.create(novaTransacao);
    }

    @Test
    public void createSaqueSemLimiteSufienteDeveLancarExcecao(){

        Transacao transacaoSaque = TransacaoBuilder.create().get();

        transacaoSaque.setValor(1501.00 * (-1));
        transacaoSaque.setConta(this.conta);
        this.conta.setSaldo(SALDO);
        this.conta.setLimiteSaqueDiario(LIMITE_SAQUE_DIARIO);

        when(contaRepository.findByIdAndPessoaUsuario(CONTA_ID, this.usuario)).thenReturn(Optional.of(this.conta));
        when(contaRepository.findSaquesHoje(any(Conta.class))).thenReturn(Double.valueOf(0));

        exception.expect(DataIntegrityException.class);
        exception.expectMessage("Valor da transacao excede o limite diario para saque");

        transacaoService.create(transacaoSaque);
    }

    @Test
    public void delete(){

        this.usuario.grantAuthority(Role.ROLE_ADMIN);

        Transacao outraTransacao = TransacaoBuilder.create().get();
        outraTransacao.setId(5);

        when(transacaoRepository.findById(anyInt())).thenReturn(Optional.of(outraTransacao));
        doNothing().when(transacaoRepository).delete(any(Transacao.class));

        transacaoService.delete(outraTransacao.getId());

        verify(transacaoRepository, times(1)).findById(outraTransacao.getId());
        verify(transacaoRepository, times(1)).delete(outraTransacao);
    }

    @Test
    public void extratoPorPeriodo(){

        Transacao transacao1 = TransacaoBuilder.create().comConta().get();

        transacao1.setValor(100.00);
        transacao1.setConta(this.conta);

        Page<Transacao> transacoes = new PageImpl<Transacao>(Arrays.asList(transacao1));
        when(contaRepository.findByIdAndPessoaUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(this.conta));
        when(transacaoRepository.findAllByContaAndDataTransacaoBetween(
                any(Conta.class),
                any(Date.class),
                any(Date.class),
                any(PageRequest.class))
        ).thenReturn(transacoes);

        Page<Transacao> extrato = transacaoService.extratoPorPeriodo(
                CONTA_ID,
                "2018-09-01",
                "2018-09-30", 0);

        verify(contaRepository, times(1)).findByIdAndPessoaUsuario(CONTA_ID, this.usuario);
        verify(transacaoRepository, times(1)).findAllByContaAndDataTransacaoBetween(
                any(Conta.class),
                any(Date.class),
                any(Date.class),
                any(PageRequest.class));
    }
}
