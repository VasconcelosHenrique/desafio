package com.vh.desafio.services;

import com.vh.desafio.data.ContaBuilder;
import com.vh.desafio.data.PessoaBuilder;
import com.vh.desafio.models.Conta;
import com.vh.desafio.models.Pessoa;
import com.vh.desafio.models.Usuario;
import com.vh.desafio.models.enums.Role;
import com.vh.desafio.repositories.ContaRepository;
import com.vh.desafio.repositories.PessoaRepository;
import com.vh.desafio.repositories.UsuarioRepository;
import com.vh.desafio.services.exceptions.DataIntegrityException;
import com.vh.desafio.services.exceptions.ObjectNotFoundException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ContaServiceTest {

    @SpyBean
    private ContaService contaService;

    @SpyBean
    private UsuarioService usuarioService;

    @SpyBean
    private PessoaService pessoaService;

    @MockBean
    private ContaRepository contaRepository;

    @MockBean
    private PessoaRepository pessoaRepository;

    @MockBean
    private UsuarioRepository usuarioRepository;

    @Rule
    public ExpectedException exception;

    private Usuario usuario;

    private Pessoa pessoa;

    private Conta conta;

    private static Authentication authentication;

    private static SecurityContext securityContext;

    private static final Integer ID                 = 2;
    private static final Integer PESSOA_ID          = 11;
    private static final Integer USUARIO_ID         = 10;
    private static final Double LIMITE_SAQUE_DIARIO = 1500.00;

    public ContaServiceTest(){

        exception       = ExpectedException.none();
        authentication  = mock(Authentication.class);
        securityContext = mock(SecurityContext.class);

        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    @Before
    public void setUp(){

        conta   = ContaBuilder.create().comPessoa().get();
        pessoa  = conta.getPessoa();
        usuario = pessoa.getUsuario();

        conta.setId(ID);
        pessoa.setId(PESSOA_ID);
        usuario.setId(USUARIO_ID);
        usuario.grantAuthority(Role.ROLE_USER);

        when(SecurityContextHolder.getContext().getAuthentication().getPrincipal()).thenReturn(usuario);
    }

    @Test
    public void findContaQuePertenceAoUsuarioLogado(){

        when(contaRepository.findByIdAndPessoaUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(this.conta));

        Conta retornada = contaService.find(PESSOA_ID);

        assertEquals(this.conta, retornada);
        verify(contaRepository, times(1)).findByIdAndPessoaUsuario(PESSOA_ID, usuario);
        verify(usuarioService, times(1)).authenticated();
        verifyNoMoreInteractions(contaRepository);
    }

    @Test
    public void findOutraContaSemPermissaoDeveLancarExcecao(){

        exception.expect(ObjectNotFoundException.class);
        exception.expectMessage("Conta nao encontrada");

        contaService.find(2);
    }

    @Test
    public void findOutraContaComAdmLogado(){

        this.usuario.grantAuthority(Role.ROLE_ADMIN);

        Conta outraConta = ContaBuilder.create().get();
        outraConta.setId(2);

        when(contaRepository.findById(anyInt())).thenReturn(Optional.of(outraConta));

        Conta retornada = contaService.find(outraConta.getId());

        assertEquals(outraConta, retornada);
        verify(contaRepository, times(1)).findById(outraConta.getId());
        verify(usuarioService, times(1)).authenticated();
        verifyNoMoreInteractions(contaRepository);
    }

    @Test
    public void create(){

        when(usuarioRepository.findById(anyInt())).thenReturn(Optional.of(this.usuario));
        when(pessoaRepository.findByIdAndUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(this.pessoa));
        when(contaRepository.save(any(Conta.class))).thenReturn(this.conta);

        contaService.create(this.conta);

        verify(contaRepository, times(1)).save(this.conta);
        verify(pessoaService, times(1)).find(anyInt());
    }

    @Test
    public void createSemPessoaDeveLancarExcecao(){

        Conta outraConta = ContaBuilder.create().get();

        exception.expect(DataIntegrityException.class);
        exception.expectMessage("Nenhuma pessoa foi informada");

        contaService.create(outraConta);
    }

    @Test
    public void createContaEPessoaSemUsuarioDeveLancarExcecao(){

        Conta outraConta   = ContaBuilder.create().get();
        Pessoa outraPessoa = PessoaBuilder.create().get();

        outraConta.setPessoa(outraPessoa);

        when(pessoaRepository.findByIdAndUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(outraPessoa));

        exception.expect(DataIntegrityException.class);
        exception.expectMessage("Nenhum usuario foi informado");

        contaService.create(outraConta);
    }

    @Test
    public void update(){

        Conta novaConta = ContaBuilder.clone(this.conta).get();

        when(pessoaRepository.findByIdAndUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(this.pessoa));
        when(contaRepository.findByIdAndPessoaUsuario(anyInt(), any(Usuario.class))).thenReturn(Optional.of(this.conta));
        when(contaRepository.save(any(Conta.class))).thenReturn(novaConta);

        Conta retornada = contaService.update(this.conta.getId(), novaConta);

        assertEquals(novaConta, retornada);
        verify(contaRepository, times(1)).save(novaConta);
    }

    @Test
    public void delete(){

        this.usuario.grantAuthority(Role.ROLE_ADMIN);

        Conta outraConta = ContaBuilder.create().get();
        outraConta.setId(5);

        when(contaRepository.findById(anyInt())).thenReturn(Optional.of(outraConta));
        doNothing().when(contaRepository).delete(any(Conta.class));

        contaService.delete(outraConta.getId());

        verify(contaRepository, times(1)).findById(outraConta.getId());
        verify(contaRepository, times(1)).delete(outraConta);
    }

    @Test
    public void saldoLimiteSaqueDiario(){

        Conta outraConta = ContaBuilder.create().get();
        outraConta.setId(5);
        outraConta.setLimiteSaqueDiario(LIMITE_SAQUE_DIARIO);

        Double totalSaques = 450.00;

        when(contaRepository.findSaquesHoje(outraConta)).thenReturn(totalSaques * (-1));

        Double expected         = Double.valueOf(LIMITE_SAQUE_DIARIO - totalSaques);
        Double saldoLimiteSaque = contaService.saldoLimiteSaqueDiario(outraConta);

        assertEquals(expected, saldoLimiteSaque);
        verify(contaRepository, times(1)).findSaquesHoje(outraConta);
    }
}
